<?php

use App\Keyword;

class KeywordTableSeeder extends ApiTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $keywords = $this->apiService->getKeywords();
        foreach ($keywords as $keyword) {
            Keyword::create([
                "id" => $keyword["KeywordId"],
                "value" => $keyword["KeywordValue"],
            ]);
        }
    }
}
