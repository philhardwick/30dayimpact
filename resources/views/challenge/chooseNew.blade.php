@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col s12 m12">
            <h2>Choose a new challenge</h2>
            <p>Each challenge is attached to a question in the EQLS data survey and aimed to improve the
            life of yourself or those around you. Each heading is a question with the results below and
                a table of challenges below that.
            </p>
            @foreach ($variables as $variable)
            <div class="row">
                <div class="col s12 m12">
                    @include('variable.questionAnswer')
                    @include('challenge.collection', ['challenges' => $variable->challenges])
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection