<?php

namespace App\Http\Middleware;

use App\Attempt;
use App\Challenge;
use Closure;
use Illuminate\Support\Facades\Hash;

class AuthoriseByQueryString
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $attemptId = $request->route('one');
        $attempt = Attempt::find($attemptId);
        if (!Hash::check($attempt->user->email.$attempt->comment.$attempt->challenge->description,
            $request->query('key'))) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('/');
            }
        }

        return $next($request);
    }
}
