@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col s12 m10 offset-m1">
            <h3>Register</h3>
            @include('generic.errors')

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="input-field col s12">
                        <input id="name" type="text" class="validate" name="name" value="{{ old('name') }}">
                        <label for="name">Name</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="email" type="email" class="validate" name="email"
                               value="{{ Request::input('email') != null ? Request::input('email') : old('email') }}">
                        <label for="email">Email</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="password" type="password" class="validate" name="password">
                        <label for="password">Password</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="password_confirmation" type="password" class="validate" name="password_confirmation">
                        <label for="password_confirmation">Confirm Password</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <select name="country">
                            @foreach ($countries as $country)
                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                            @endforeach
                        </select>
                        <label>Country you live in</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <button class="btn waves-effect waves-light" type="submit">Register</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
