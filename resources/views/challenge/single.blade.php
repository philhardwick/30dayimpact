@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <h3>{{ $challenge->title }}</h3>

        <div class="row">
            <div class="col m6">

            <p>{{ $challenge->description }}</p>

            <h5 class="light">In order to complete the challenge...</h5>

            <p>{{ $challenge->to_complete }}</p>

            </div>
            <div class="col m6">
                @include('variable.questionAnswer', ['variable' => $challenge->variable])
            </div>
        </div>
        <!-- If the user is the verifier of this challenge and it is waiting to be verified -->
        @if($attemptToVerify)
            <h5 class="light">You have been asked to verify this challenge</h5>
            <p>Only approve this challenge if you think that {{ $attemptToVerify->user->name }} has fulfilled
                everything needed for the challenge to have the proper impact in the community. Take into account
                the comment, the photo the time it was finished and started.
            </p>
            <div class="row">
                <div class="col s12 m8 offset-m2 l6 offset-l3">
                    @include('attempt.verify', ['attempt' => $attemptToVerify])
                </div>
            </div>
        @else
            <!-- If this user has attempts for this challenge -->
            @if($myMostRecentAttempt)
                <div class="row">
                    <div class="col s12 m8 offset-m2 l6 offset-l3">
                        @include('attempt.myAttempt', ['attempt' => $myMostRecentAttempt])
                    </div>
                </div>
            @else
            <form action="{{ action('AttemptController@postNewAttempt') }}" method="post">
                {!! csrf_field() !!}
                <input type="hidden" name="challenge_id" value="{{ $challenge->id }}">
                <button class="btn waves-effect waves-light" type="submit" name="action">Challenge Accepted!
                    <i class="mdi mdi-run right"></i>
                </button>
            </form>
            @endif
        @endif
    </div>
</div>
@endsection


