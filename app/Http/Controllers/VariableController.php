<?php

namespace App\Http\Controllers;

use App\Api\ApiService;
use App\Variable;
use Illuminate\Http\Request;

use App\Http\Requests;

class VariableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param ApiService $apiService
     * @return Response
     */
    public function getIndex(Request $request, ApiService $apiService)
    {
        $variables = Variable::where('topic_id', $request->input('topicId'))->get();
        $data = $apiService->getDataForVariables($variables);
        return view('variable.list', [
            'variables' => $variables,
            'data' => $data,
        ]);
    }
}
