@extends('app')

@section('content')
<div class="container">
    @foreach ($variables as $variable)
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('variable.questionAnswer')
            <form method="GET" action="{{ url('challenge/create') }}">
                <input type="hidden" name="variableId" value="{{ $variable->id }}">
                <div class="row">
                    <button class="btn waves-effect waves-light" type="submit">
                        Create a challenge about this data
                    </button>
                </div>
            </form>
        </div>
    </div>
    @endforeach
</div>
@endsection