<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    /**
     * Get the variable for the category
     */
    public function variable()
    {
        return $this->belongsTo('App\Variable');
    }
}
