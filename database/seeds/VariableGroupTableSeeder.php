<?php

use App\VariableGroup;

class VariableGroupTableSeeder extends ApiTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $variableGroups = $this->apiService->getVariableGroups();
        foreach ($variableGroups as $variableGroup) {
            VariableGroup::create([
                "id" => $variableGroup["VariableGroupId"],
                "value" => $variableGroup["VariableGroupValue"],
            ]);
        }
    }
}
