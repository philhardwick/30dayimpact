<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AttemptsTest extends TestCase
{
    use DatabaseTransactions;

    public function testStartingAnAttempt() {
        $user = factory(App\User::class)->create();

        $challenge = factory(App\Challenge::class)->create();

        $this->actingAs($user)
            ->visit('/challenge/show/'.$challenge->id)
            ->press('Challenge Accepted!')
            ->seePageIs('/user/challenges');

        $this->seeInDatabase('attempts', [
            'approval_status' => 'unfinished',
            'challenge_id' => $challenge->id,
            'user_id' => $user->id,
        ]);
    }

    public function testTryingToStartMoreThanOneAttemptADay() {
        $user = factory(App\User::class)->create();

        $challenge = factory(App\Challenge::class)->create();
        $challenge2 = factory(App\Challenge::class)->create();

        $this->actingAs($user)
            ->visit('/challenge/show/'.$challenge->id)
            ->press('Challenge Accepted!')
            ->seePageIs('/user/challenges')
            ->visit('/challenge/show/'.$challenge2->id)
            ->press('Challenge Accepted!')
            ->seePageIs('/attempt/no-start');

        $this->notSeeInDatabase('attempts', [
            'approval_status' => 'unfinished',
            'challenge_id' => $challenge2->id,
            'user_id' => $user->id,
        ]);
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testFinishingAttempt()
    {
        $user = factory(App\User::class)->create();

        $challenge = factory(App\Challenge::class)->create();

        Mail::shouldReceive('send')
            ->once();

        $this->startAndFinishAttempt($user, $challenge, "new@email.com");

        $this->seeInDatabase('attempts', [
            'approval_status' => 'pending',
            'comment' => 'comment',
            'challenge_id' => $challenge->id,
            'user_id' => $user->id,
            'approver_email' => 'new@email.com'
        ]);
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testAChallengeCreatedByAUserWillAddToImpactScoreEveryTimeANewUserStartsThatChallenge()
    {
        $user = factory(App\User::class)->create();
        $user2 = factory(App\User::class)->create();

        $challenge = factory(App\Challenge::class)->create();
        $challenge->owner_id = $user->id;
        $challenge->save();

        $this->actingAs($user2)
            ->visit('/challenge/show/'.$challenge->id)
            ->press('Challenge Accepted!');

        $this->seeInDatabase('users', [
            'email' => $user->email,
            'impact_score' => $user->impact_score+1
        ]);
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testApprovingAnAttempt()
    {
        $user = factory(App\User::class)->create();
        $user2 = factory(App\User::class)->create();

        Mail::shouldReceive('send')
            ->never();

        $challenge = factory(App\Challenge::class)->create();

        $this->startAndFinishAttempt($user, $challenge, $user2->email);

        $this->actingAs($user2)
            ->visit('/user/profile/')
            ->see($challenge->title)
            ->click($challenge->title)
            ->press('Approve')
            ->seePageIs('/user/profile');

        $this->seeInDatabase('attempts', [
            'approval_status' => 'approved',
            'comment' => 'comment',
            'challenge_id' => $challenge->id,
            'user_id' => $user->id,
            'approver_email' => $user2->email
        ]);

        $this->seeInDatabase('users', [
            'email' => $user->email,
            'impact_score' => $user->impact_score+1
        ]);
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testRejectingAnAttempt()
    {
        $user = factory(App\User::class)->create();
        $user2 = factory(App\User::class)->create();

        Mail::shouldReceive('send')
            ->never();

        $challenge = factory(App\Challenge::class)->create();

        $this->startAndFinishAttempt($user, $challenge, $user2->email);

        $this->actingAs($user2)
            ->visit('/user/profile/')
            ->see($challenge->title)
            ->click($challenge->title)
            ->press('Reject')
            ->seePageIs('/user/profile');

        $this->seeInDatabase('attempts', [
            'approval_status' => 'rejected',
            'comment' => 'comment',
            'challenge_id' => $challenge->id,
            'user_id' => $user->id,
            'approver_email' => $user2->email
        ]);

        $this->seeInDatabase('users', [
            'email' => $user->email,
            'impact_score' => $user->impact_score
        ]);
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testApprovingMultipleAttemptsFromDifferentUsers()
    {
        $user = factory(App\User::class)->create();
        $user2 = factory(App\User::class)->create();
        $user3 = factory(App\User::class)->create();

        Mail::shouldReceive('send')
            ->never();

        $challenge = factory(App\Challenge::class)->create();

        $this->startAndFinishAttempt($user, $challenge, $user3->email);
        $this->startAndFinishAttempt($user2, $challenge, $user3->email);

        $this->actingAs($user3)
            ->visit('/user/profile/')
            ->see($challenge->title)
            ->click($challenge->title)
            ->see('finished at')
            ->see('comment')
            ->dontSee('Resend')
            ->press('Approve')
            ->seePageIs('/user/profile')
            ->see($challenge->title)
            ->click($challenge->title)
            ->press('Approve')
            ->seePageIs('/user/profile');
    }

    public function testUserTryingToSetOwnEmailAsApprover() {
        $user = factory(App\User::class)->create();
        $challenge = factory(App\Challenge::class)->create();

        $this->actingAs($user)
            ->visit('/challenge/show/'.$challenge->id)
            ->press('Challenge Accepted!')
            ->visit('/challenge/show/'.$challenge->id)
            ->click('Finished?')
            ->attach(base_path('tests/footer.png'), 'photo')
            ->type('comment', 'comment')
            ->type($user->email, 'email')
            ->press('Submit')
            ->see('There were some problems with your input');
    }

    public function testStartingAnAttemptOnDay31() {
        $user = factory(App\User::class)->create();
        $challenge = factory(App\Challenge::class)->create();
        $challenge2 = factory(App\Challenge::class)->create();

        $this->startAndFinishAttempt($user, $challenge, User::all()->first()->email);

        $firstAttempt = $user->attempts()->oldest()->first();
        $firstAttempt->started = Carbon::now()->addDays(-31);
        $firstAttempt->save();

        $this->actingAs($user)
            ->visit('/challenge/show/'.$challenge2->id)
            ->press('Challenge Accepted!')
            ->see('You have completed the 30 day challenge');
    }

    public function testStartingTwoAttemptsOnSameDay() {
        $user = factory(App\User::class)->create();
        $challenge = factory(App\Challenge::class)->create();
        $challenge2 = factory(App\Challenge::class)->create();

        $this->startAndFinishAttempt($user, $challenge, User::all()->first()->email);

        $this->actingAs($user)
            ->visit('/challenge/show/'.$challenge2->id)
            ->press('Challenge Accepted!')
            ->see('You cannot start more than one challenge on the same day');
    }

    public function testSeeingAChallengeBecauseItsSharedWithAQueryStringKey() {
        $user = factory(App\User::class)->create();
        $challenge = factory(App\Challenge::class)->create();

        $this->startAndFinishAttempt($user, $challenge, User::all()->first()->email);
        $attempt = $challenge->attempts()->mostRecent()->first();

        $this->visit('/attempt/show/'.$attempt->id)
            ->seePageIs('/');

        $key = bcrypt($attempt->user->email.$attempt->comment.$attempt->challenge->description);
        $this->visit('/attempt/show/'.$attempt->id.'?key='.$key)
            ->see($attempt->comment);
    }

    /**
     * @param $user
     * @param $challenge
     * @param $user2
     * @return $this
     */
    private function startAndFinishAttempt($user, $challenge, $email)
    {
        return $this->actingAs($user)
            ->visit('/challenge/show/' . $challenge->id)
            ->press('Challenge Accepted!')
            ->visit('/challenge/show/' . $challenge->id)
            ->click('Finished?')
            ->attach(base_path('tests/footer.png'), 'photo')
            ->type('comment', 'comment')
            ->type($email, 'email')
            ->press('Submit')
            ->seePageIs('/user/challenges');
    }
}
