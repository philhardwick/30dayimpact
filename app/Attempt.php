<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Attempt extends Model
{

    protected $dates = ['created_at', 'updated_at', 'started', 'finished'];

    public function scopeUnfinished($query)
    {
        return $query->where('approval_status', 'unfinished');
    }

    public function scopeFinished($query)
    {
        return $query->where('approval_status', 'pending');
    }

    /**
     * Syntatic sugar so I can call it unverified
     * @param $query
     * @return mixed
     */
    public function scopeUnverified($query) {
        return $this->scopeFinished($query);
    }

    public function scopeVerified($query)
    {
        return $query->whereIn('approval_status', ['approved', 'rejected']);
    }

    public function scopeApproved($query)
    {
        return $query->where('approval_status', 'approved');
    }

    public function scopeForCurrentUser($query)
    {
        return $query->where('user_id', Auth::id());
    }

    public function scopeForUser($query, $id)
    {
        return $query->where('user_id', $id);
    }

    public function scopeMostRecent($query)
    {
        return $query->orderBy('started', 'desc');
    }

    public function scopeOldest($query)
    {
        return $query->orderBy('started', 'asc');
    }

    public function scopeForUserToVerify($query, $user_id)
    {
        return $query->where('approver_email', User::find($user_id)->email);
    }

    /**
     * Get the user for the attempt
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the user for the attempt
     */
    public function challenge()
    {
        return $this->belongsTo('App\Challenge');
    }
}
