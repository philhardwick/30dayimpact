<h5>{{ $variable->question }}</h5>
<div class="row">
    <div class="col s12">
        <ul>
        @forelse ($variable->categories as $category)
            @if (isset($data[$variable->id][$category->value]))
            <li>
                {{ $category->label }}&nbsp;{{ $data[$variable->id][$category->value] }}%&nbsp;
                <div class="progress">
                    <div class="determinate" style="width: {{ $data[$variable->id][$category->value] }}%"></div>
                </div>
            </li>
            @endif
        @empty
            @foreach ($data[$variable->id] as $number => $numericalAnswer)
            <li>
                {{ $number }}&nbsp;{{ $numericalAnswer }}%&nbsp;
                <div class="progress">
                    <div class="determinate" style="width: {{ $numericalAnswer }}%"></div>
                </div>
            </li>
            @endforeach
        @endforelse
        </ul>
    </div>
</div>