@extends('attempt.single')

@section('cardContent')
    <p>You started at {{ $attempt->started }} and finished at {{ $attempt->finished }}</p>
    <p>
        This attempt has been sent to {{ $attempt->approver_email }}
    </p>
@endsection

@section('cardAction')
    @if($attempt->user_id == Auth::id() && !DB::table('users')->where('email', $attempt->approver_email)->exists())
        <a href="{{ url('attempt/resend/'.$attempt->id) }}" class="">Resend</a>
    @else
        @if (count($sharingLinks) > 0)
            <a href="{{ $sharingLinks['facebook'] }}" class="btn-flat waves-effect waves-light">
                <i class="mdi mdi-facebook"></i>
                <span class="hide">facebook</span>
            </a>
            <a href="{{ $sharingLinks['twitter'] }}" class="btn-flat waves-effect waves-light">
                <i class="mdi mdi-twitter"></i>
                <span class="hide">twitter</span>
            </a>
            <a href="{{ $sharingLinks['gplus'] }}" class="btn-flat waves-effect waves-light">
                <i class="mdi mdi-google-plus"></i>
                <span class="hide">gplus</span>
            </a>
        @endif
    @endif
@endsection