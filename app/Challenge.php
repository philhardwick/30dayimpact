<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Challenge extends Model
{
    protected $fillable = ['title', 'description', 'to_complete', 'owner_id', 'approval_status'];

    /**
     * Get the variable for the challenge
     */
    public function variable()
    {
        return $this->belongsTo('App\Variable');
    }

    /**
     * Get the variable for the challenge
     */
    public function owner()
    {
        return $this->belongsTo('App\User', 'owner_id');
    }

    /**
     * Get the attempts for the challenge
     */
    public function attempts()
    {
        return $this->hasMany('App\Attempt');
    }

    public function scopeCreatedByCurrentUser($query)
    {
        return $query->where('owner_id', Auth::id());
    }
}
