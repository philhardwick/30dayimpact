@if (count($errors) > 0)
<div class="materialize-red">
    <i class="mdi mdi-alert"></i><strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif