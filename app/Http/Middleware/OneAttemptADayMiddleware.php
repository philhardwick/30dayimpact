<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class OneAttemptADayMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $mostRecentAttempt = Auth::user()->attempts()->mostRecent()->first();
        if ($mostRecentAttempt && Carbon::now()->isSameDay($mostRecentAttempt->started)) {
            return redirect('/attempt/no-start');
        }
        return $next($request);
    }
}
