@extends('app')

@section('content')
<div class="container">
    <h1>Create your Challenge</h1>

    <div class="row">
        @include('generic.errors')
        <form class="col s12" method="post" action="{{ url('challenge/store') }}">
            {!! csrf_field() !!}
            <input type="hidden" name="variable_id" value="{{ $variable->id }}">
            <div class="row">
                <div class="input-field col s12">
                    <input id="challenge_title" type="text" class="validate" name="title" value="{{ old('title') }}">
                    <label for="challenge_title">Challenge Title</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <textarea id="challenge_description" class="materialize-textarea" name="description">
                        {{ old('description') }}
                    </textarea>
                    <label for="challenge_description">Challenge Description</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <textarea id="to_complete" class="materialize-textarea" name="to_complete">
                        {{ old('to_complete') }}
                    </textarea>
                    <label for="to_complete">To Complete (Let them know what they have to do to complete the
                        challenge)</label>
                </div>
            </div>
            <div class="row">
                <button class="btn waves-effect waves-light" type="submit">Create
                    <i class="mdi mdi-send"></i>
                </button>
            </div>
        </form>
    </div>
</div>
@endsection