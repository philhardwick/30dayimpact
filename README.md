# 30DayImpact #

An application which uses the UK Data Service's API for the EU quality of life survey results. The application is built in PHP and Laravel as the web framework, the application can be configured to use MySQL or PostgreSQL.

### How do I get set up? ###

* Deploy to a web server like apache and point the document root to the public folder.
* Configure the database credentials and host through the .env file or directly in the config/database.php file. 
* Run php artisan migrate --seed to initialise the database.

If the application isn't returning the home page try adding index.php to the url. This indicates theres an issue with the .htaccess configuration.

A running instance is already set up at  [http://thirtydayimpact.herokuapp.com](http://thirtydayimpact.herokuapp.com).