<?php

use App\Challenge;
use Illuminate\Database\Seeder;

class ChallengeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Challenge::create([
            'title' => "Offer to drive a friend to see their relatives",
            'description' => "Give a friend a lift to see a relative who is not easily accessible or frequently visited.",
            'to_complete' => "Upload a photo of the time at the relatives house",
            'approval_status' => 'approved',
            'variable_id' => 25,
        ]);

        Challenge::create([
            'title' => "Offer help to a neighbour",
            'description' => "If you see a neighbour doign chores or needing help go offer it, otherwise go offer your help or letting them know they can ask to borrow things if needed.",
            'to_complete' => "Upload a photo of knocking on their door or doing the chore",
            'approval_status' => 'approved',
            'variable_id' => 26,
        ]);

        Challenge::create([
            'title' => "Pay for a long distance call",
            'description' => "Pay for the phone bill for a friend to speak to a far away relative or friend, if feeling extra generous pay for an international sim and load it with enough money to last a month.",
            'to_complete' => "Upload a photo of paying the phone bill or buying the sim card.",
            'approval_status' => 'approved',
            'variable_id' => 29,
        ]);

        Challenge::create([
            'title' => "Help around the house",
            'description' => "Help do the chores or any odd jobs around the house for a friend or someone you know who needs it.",
            'to_complete' => "Upload a photo of doing the chore.",
            'approval_status' => 'approved',
            'variable_id' => 31,
        ]);

        Challenge::create([
            'title' => "Listen carefully",
            'description' => "When speaking to someone or making small talk, make a point of asking about their family and being interested, listen and be attentive and offer advice if needed.",
            'to_complete' => "Comment what you talked about.",
            'approval_status' => 'approved',
            'variable_id' => 32,
        ]);

        Challenge::create([
            'title' => "Proof read a CV",
            'description' => "Help a friend find a job by proof reading their CV",
            'to_complete' => "Upload a photo of your notes on the CV.",
            'approval_status' => 'approved',
            'variable_id' => 33,
        ]);

        Challenge::create([
            'title' => "Chronic health relief",
            'description' => "Organise a day of fun/pampering/spoiling for someone you know who has chronic illness.",
            'to_complete' => "Upload a photo of the day plan or one of the activities you did.",
            'approval_status' => 'approved',
            'variable_id' => 37,
        ]);

        Challenge::create([
            'title' => "Free clean water",
            'description' => "If your immediate neighbourhood doesn't have clean drinking water, buy a large number fo bottles and give them away.",
            'to_complete' => "Upload a photo of giving them away or buying them.",
            'approval_status' => 'approved',
            'variable_id' => 50,
        ]);

        Challenge::create([
            'title' => "Drive to doctors",
            'description' => "Offer to drive a friend to the doctors.",
            'to_complete' => "Upload a photo of the trip together.",
            'approval_status' => 'approved',
            'variable_id' => 55,
        ]);

        Challenge::create([
            'title' => "Go to the doctors with a friend",
            'description' => "Offer to wait with them in the waiting room.",
            'to_complete' => "Upload a photo of going to the doctors.",
            'approval_status' => 'approved',
            'variable_id' => 57,
        ]);

        Challenge::create([
            'title' => "Babysit for appointment",
            'description' => "Offer to babysit children so a parent you know can go to the doctors",
            'to_complete' => "Upload a photo of the trip together.",
            'approval_status' => 'approved',
            'variable_id' => 55,
        ]);

        Challenge::create([
            'title' => "Sign a petition and share it",
            'description' => "Find a petition of an issue you care about. Use a petition website such as change.org to find one. Sign up to support the petition and then share it on social media.",
            'to_complete' => "Upload screenshot of sharing your petition",
            'approval_status' => 'approved',
            'variable_id' => 116,
        ]);

        Challenge::create([
            'title' => "Create a petition and share it",
            'description' => 'Think of an issue you\'re passionate about which needs to be addressed in government. Start your petition online and think about what you want changed and what you want to be considered in parliament.' ,
            'to_complete' => 'Upload screenshot of sharing your petition',
            'approval_status' => 'approved',
            'variable_id' => 116,
        ]);

        Challenge::create([
            'title' => 'Write or email your local MP',
            'description' => 'Think of a local issue you want to be changed. Word your letter and include what you want to be done.',
            'to_complete' => 'Upload screenshot of your email (blur out some of the email if you want, but make sure the address field is clear) or a photo of you sending the letter.',
            'approval_status' => 'approved',
            'variable_id' => 117,
        ]);

        Challenge::create([
            'title' => 'Join a local group',
            'description' => "Use one of your interests or hobbies and go along to a local group for that hobby or interest. ",
            'to_complete' => 'Upload a photo of your time at the group.',
            'approval_status' => 'approved',
            'variable_id' => 118,
        ]);

        Challenge::create([
            'title' => 'Take up a new sport',
            'description' => "Go along to a sports club of a sport you've always wanted to try.",
            'to_complete' => 'Upload a photo of your time at the club.',
            'approval_status' => 'approved',
            'variable_id' => 107,
        ]);

        Challenge::create([
            'title' => 'Organise a social activity',
            'description' => "Avoid going to the pub and do something fun and unusual like raft building or geo-caching with a group of friends. Organise it for a large group of people and include as many as possible.",
            'to_complete' => 'Upload a photo of your time doing the activity, a screenshot of a Facebook event page or similar',
            'approval_status' => 'approved',
            'variable_id' => 108,
        ]);

        Challenge::create([
            'title' => 'Volunteer a day',
            'description' => "Find a charity which benefits the community, could be a youth club, an art gallery or a homeless shelter, and volunteer there. Do it at least once but maybe try volunteer once a month.",
            'to_complete' => "Upload a photo showing you're volunteering",
            'approval_status' => 'approved',
            'variable_id' => 109,
        ]);

        Challenge::create([
            'title' => 'Attend a peaceful demonstration',
            'description' => "Think of an issue you feel passionate about and find a local demonstration which is aiming to make the issue better. Join their demonstration on the day.",
            'to_complete' => "Upload a photo of you at the demonstration",
            'approval_status' => 'approved',
            'variable_id' => 115,
        ]);

        Challenge::create([
            'title' => 'Uncomplicate things',
            'description' => "Think of a friend who has a complicated life and ask what you can do to help.",
            'to_complete' => "Comment what you did and upload a picture of you doing it.",
            'approval_status' => 'approved',
            'variable_id' => 115,
        ]);

        Challenge::create([
            'title' => 'Encouraging 7 days',
            'description' => "For a whole week, encourage one person a day. Tell them something you love about them make sure they know they don't have to say anything back! This could be through notes, messages, emails or meeting up.",
            'to_complete' => "Upload a picture of an encouragement",
            'approval_status' => 'approved',
            'variable_id' => 120,
        ]);

        Challenge::create([
            'title' => 'Encouraging 7 days (for work)',
            'description' => "For a whole week, encourage one person a day for what they do as a job. No matter if they're working, volunteering or looking for employment, make sure they know they are valuable! This could be through notes, messages, emails or meeting up.",
            'to_complete' => "Upload a picture of an encouragement",
            'approval_status' => 'approved',
            'variable_id' => 121,
        ]);

        Challenge::create([
            'title' => 'Invite some neighbours for dinner',
            'description' => "Organise an evening where you invite your close geographical neighbours for some food.",
            'to_complete' => "Upload a picture of the dinner together.",
            'approval_status' => 'approved',
            'variable_id' => 122,
        ]);

        Challenge::create([
            'title' => "Food Bank",
            'description' => "If there is a food bank in your country, find a list of their needed items and buy 4 of those items each week for the rest of the challenge, donating them to the food bank.",
            'to_complete' => "Show a picture of the items going to the food bank",
            'approval_status' => 'approved',
            'variable_id' => 126,
        ]);

        Challenge::create([
            'title' => "Offer to pay someone's heating bill for a month",
            'description' => "Pay a friend or neighbour's heating bill for a month.",
            'to_complete' => "Upload a picture of the money transfer or even better the transfer to their heating provider",
            'approval_status' => 'approved',
            'variable_id' => 127,
        ]);

        Challenge::create([
            'title' => "Donate unneeded furniture to a charity",
            'description' => "Look for furniture you don't need in your house and donate it to a charity.",
            'to_complete' => "Upload a picture of the donation",
            'approval_status' => 'approved',
            'variable_id' => 129,
        ]);

        Challenge::create([
            'title' => "Look after the kids",
            'description' => "Offer to babysit the children of a couple or single parent who needs a night off, offer to pay for their meal or activity too.",
            'to_complete' => "Upload a picture of you with the couple.",
            'approval_status' => 'approved',
            'variable_id' => 143,
        ]);

        Challenge::create([
            'title' => "Free up",
            'description' => "Find a friend or family member who doesn't find the time to do things they enjoy, find out why and help make the time for them.",
            'to_complete' => "Upload a picture of what you do to help them.",
            'approval_status' => 'approved',
            'variable_id' => 143,
        ]);

        Challenge::create([
            'title' => "Plan in what you love",
            'description' => "Think of your favourite activity or hobby and plan in time to do. Add it to your calender/diary so it is not forgotten or overtaken by other tasks.",
            'to_complete' => "Upload a picture of you doing your favourite thing.",
            'approval_status' => 'approved',
            'variable_id' => 143,
        ]);

        Challenge::create([
            'title' => "Share drinks/meal",
            'description' => "Invite friends or family round for a meal or drinks.",
            'to_complete' => "Upload a picture of your time together.",
            'approval_status' => 'approved',
            'variable_id' => 132,
        ]);

        Challenge::create([
            'title' => "Share your optimism for a week",
            'description' => "Notice when those around you say something negative about what's going to happen in the future and try counter that with an optimistic view of the future. Do this for 7 days and see what a difference it makes to the perspectives of those around you.",
            'to_complete' => "Get someone more pessimistic than you to verify that you've been sharing optimistic perspectives all week and upload a memorable photo from it.",
            'approval_status' => 'approved',
            'variable_id' => 140,
        ]);

        Challenge::create([
            'title' => "Help a job search",
            'description' => "Find a friend who is searching for a job and offer to review their CV and look for jobs with them. Help them identify what their dream job is.",
            'to_complete' => "Upload a photo of the job search together.",
            'approval_status' => 'approved',
            'variable_id' => 146,
        ]);

        Challenge::create([
            'title' => "DIY",
            'description' => "Find a friend who wants help with DIY and dedicate and afternoon to helping them improve their house.",
            'to_complete' => "Upload a photo of the finished DIY with a comment on what you did.",
            'approval_status' => 'approved',
            'variable_id' => 148,
        ]);

        Challenge::create([
            'title' => "Group exercise",
            'description' => "Offer to do exercise with a friend. Either take up a sport,go running or go to the gym together.",
            'to_complete' => "Upload a photo of the exercise.",
            'approval_status' => 'approved',
            'variable_id' => 156,
        ]);

        Challenge::create([
            'title' => "Cook so they don't have to",
            'description' => "Cook a meal in advance and package it so that you can give it to someone who works long hours. When they get home from work they can simply heat up your meal instead of cooking themselves.",
            'to_complete' => "Upload a photo of the packaged meal.",
            'approval_status' => 'approved',
            'variable_id' => 171,
        ]);

        Challenge::create([
            'title' => "100% housework",
            'description' => "If you don't usually do all the housework, do all of it for a week.",
            'to_complete' => "Upload a photo of doing the housework",
            'approval_status' => 'approved',
            'variable_id' => 179,
        ]);

        Challenge::create([
            'title' => "Buy a drink for someone you don't know",
            'description' => "This could be on a train, the person behind you in the queue at the coffee store or someone on the street.",
            'to_complete' => "Upload a photo of buying the two drinks.",
            'approval_status' => 'approved',
            'variable_id' => 132,
        ]);
    }
}
