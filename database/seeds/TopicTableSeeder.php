<?php

use App\Api\ApiService;
use App\Topic;

class TopicTableSeeder extends ApiTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $topics = $this->apiService->getTopics();
        foreach ($topics as $topic) {
            Topic::create([
                "id" => $topic["TopicId"],
                "name" => $topic["TopicValue"],
            ]);
        }
    }
}
