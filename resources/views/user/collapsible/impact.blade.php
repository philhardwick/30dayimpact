<li>
    <div class="collapsible-header grey-hover lighten-3">
        <i class="mdi mdi-star"></i>
        Impact: {{ $user->impact_score }}
    </div>
    <div class="collapsible-body">
        <p>You gain impact for every challenge completed and verified.
            <br>
            So far {{ $numberOfCompletedChallengesForUser }}
            {{ $numberOfCompletedChallengesForUser == 1 ? 'challenge':'challenges' }}
            has been completed by you and verified.
            <br>
            You have also had {{ $numberOfCompletedAttemptsOfChallengesCreatedByUser }}
            {{ $numberOfCompletedAttemptsOfChallengesCreatedByUser == 1 ? 'person':'people' }} finish
            a challenge you have created.
        </p>
    </div>
</li>