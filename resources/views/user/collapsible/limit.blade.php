<li>
    <div class="collapsible-header grey-hover lighten-3">
        <i class="mdi mdi-clock"></i>
        @if ($thirtyDayLimit)
        {{ $numOfDaysLeft }} {{ $numOfDaysLeft == 1 ? 'day':'days' }} left
        @else
        No time limit yet!
        @endif
    </div>
    <div class="collapsible-body">
        @if ($thirtyDayLimit)
        <p>You have until {{ $thirtyDayLimit }} to gain as much impact as possible
            <br>
            You can accept one challenge a day at most.</p>
        @else
        <p>You have thirty days from the time you start a challenge, so waste no time an make an impact now!</p>
        @endif
    </div>
</li>