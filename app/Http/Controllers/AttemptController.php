<?php

namespace App\Http\Controllers;

use App\Attempt;
use App\Challenge;
use App\Http\Requests\FinishAttemptRequest;
use App\Http\Requests\VerifyAttemptRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class AttemptController extends Controller
{
    /**
     * Create a new attempt controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['getShow']]);
        $this->middleware('oneAttemptADay', ['only' => ['postNewAttempt']]);
        $this->middleware('authByQueryString', ['only' => ['getShow']]);
        $this->middleware('thirtyDayLimit', ['only' => ['postNewAttempt']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function postNewAttempt(Request $request)
    {
        $challenge = Challenge::find($request->input("challenge_id"));
        $attempt = new Attempt();
        $attempt->started = new Carbon();
        $attempt->approval_status = 'unfinished';
        $attempt->challenge()->associate($challenge->id)
            ->user()->associate(auth()->user()->id)->save();
        $attempt->save();
        if ($challenge->owner_id) {
            $challengeOwner = User::find($challenge->owner_id);
            $challengeOwner->impact_score = $challengeOwner->impact_score + 1;
            $challengeOwner->save();
        }
        return redirect("user/challenges");
    }

    public function getFinish($id) {
        return view('attempt.finish', [
            'attempt' => Attempt::find($id),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $id
     * @param  Request $request
     * @return Response
     */
    public function postFinishAttempt(FinishAttemptRequest $request, $id)
    {
        $attempt = Attempt::find($id);
        $attempt->finished = new Carbon();
        $attempt->approval_status = 'pending';
        $attempt->comment = $request->input('comment');
        $approver_email = $request->input('email');
        $attempt->approver_email = $approver_email;
        $user = $attempt->user;
        if (!User::where('email', $approver_email)->exists()) {
            Mail::send('emails.invite', ['user' => $user, 'attempt' => $attempt], function ($m) use ($user, $approver_email) {
                $m->to($approver_email)->subject($user->name . ' has invited you to 30DayImpact!');
            });
        }
        if ($request->hasFile('photo')) {
            $uploadedPhoto = $request->file('photo');
            $uploadedPhoto->move(public_path() . Config::get('filesystems.photoUploadPath'), $uploadedPhoto->getFilename());
            $attempt->photo_url = Config::get('filesystems.photoUploadPath') . '/' . $uploadedPhoto->getFilename();
        }
        $attempt->save();
        return redirect("user/challenges");
    }

    public function postApproveAttempt(VerifyAttemptRequest $request, $id) {
        $attempt = Attempt::find($id);
        $user = $attempt->user;
        $user->impact_score = $user->impact_score + 1;
        $attempt->approval_status = 'approved';
        $user->save();
        $attempt->save();
        return redirect('user/profile');
    }

    public function postRejectAttempt(VerifyAttemptRequest $request, $id) {
        $attempt = Attempt::find($id);
        $attempt->approval_status = 'rejected';
        $attempt->save();
        return redirect('user/profile');
    }

    public function getResend($id)
    {
        $attempt = Attempt::find($id);
        $approver_email = $attempt->approver_email;
        $user = $attempt->user;
        if (!User::where('email', $approver_email)->exists()) {
            Mail::send('emails.invite', ['user' => $user, 'attempt' => $attempt], function ($m) use ($user, $approver_email) {
                $m->to($approver_email)->subject($user->name . ' has invited you to 30DayImpact!');
            });
        }
        return redirect("challenge/show/".$attempt->challenge->id);
    }

    /**
     *
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function getNoStart()
    {
        return view('attempt.noStart');
    }

    /**
     *
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function getNoStartTimeLimit()
    {
        return view('attempt.noStartTimeLimit');
    }


    public function getShow($id)
    {
        $attempt = Attempt::find($id);
        $challenge = $attempt->challenge;

        return view('challenge.single', [
            'challenge' => $challenge,
            'attemptToVerify' => false,
            'myMostRecentAttempt' => $attempt,
            'sharingLinks' => array(),
        ]);
    }
}
