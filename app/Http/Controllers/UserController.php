<?php

namespace App\Http\Controllers;

use App\Attempt;
use App\Challenge;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private $user;

    public function __construct()
    {
        $this->middleware('auth');
        $this->user = Auth::user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getProfile()
    {
        $firstAttempt = $this->user->attempts()->oldest()->first();
        $thirtyDayLimitForUser = '';
        $numOfDaysLeft = 0;
        if ($firstAttempt) {
            $thirtyDayLimitForUser = $firstAttempt->started->addDays(30)->toFormattedDateString();
            $numOfDaysLeft = Carbon::today()->diffInDays($firstAttempt->started->addDays(30));
        }
        $numberOfCompletedAttemptsOfChallengesCreatedByUser = Challenge::createdByCurrentUser()->get()
            ->filter(function ($challenge) {
                return $challenge->attempt;
            })->map(function ($challenge) {
                return $challenge->attempt;
            })->filter(function ($attempt) {
                return $attempt->approval_status == 'approved';
            })->count();
        return view('user.profile', [
            'user' => $this->user,
            'thirtyDayLimit' => $thirtyDayLimitForUser,
            'unfinishedChallenges' => Attempt::unfinished()
                ->forCurrentUser()->get()
                ->map(function ($attempt) {
                    return $attempt->challenge;
                }),
            'challengesToVerify' => Attempt::forUserToVerify($this->user->id)->finished()->get()
                ->map(function ($attempt) {
                    return $attempt->challenge;
                }),
            'numberOfCompletedChallengesForUser' => $this->user->attempts()->approved()->get()->count(),
            'numberOfCompletedAttemptsOfChallengesCreatedByUser' => $numberOfCompletedAttemptsOfChallengesCreatedByUser,
            'numOfDaysLeft' => $numOfDaysLeft,
        ]);
    }

    public function getChallenges() {
        return view('user.challenges', [
            'unfinishedChallenges' => Attempt::unfinished()
                ->forCurrentUser()->get()
                ->map(function ($attempt) {
                    return $attempt->challenge;
                }),
            'pendingChallenges' => Attempt::finished()
                ->forCurrentUser()->get()
                ->map(function ($attempt) {
                    return $attempt->challenge;
                }),
            'verifiedChallenges' => Attempt::verified()
                ->forCurrentUser()->get()
                ->map(function ($attempt) {
                    return $attempt->challenge;
                }),
        ]);
    }

}
