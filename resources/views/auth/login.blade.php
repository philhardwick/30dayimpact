@extends('app')

@section('content')
<div class="container">
    <div class="section">
        <h5>Login</h5>
    </div>
    <div class="row">
        @include('generic.errors')
        <form class="col s12" role="form" method="POST" action="{{ url('/auth/login') }}">
            {!! csrf_field() !!}
            <div class="row">
                <div class="input-field col s12">
                    <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}">
                    <label for="email">Email</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="password" type="password" class="validate" name="password">
                    <label for="password">Password</label>
                </div>
            </div>
            <div class="">
                <p>
                    <input type="checkbox" id="remember_me" name="remember"/>
                    <label for="remember_me">Remember Me</label>
                </p>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn waves-effect waves-light" type="submit" name="action">Submit</button>
                </div>
                <div class="input-field col s12">
                    <p class="light">If you haven't got an account</p>
                    <a href="{{ url('auth/register') }}" class="btn-flat waves-effect waves-light">Register</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
