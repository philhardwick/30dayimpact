<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Country;

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
        'impact_score' => $faker->numberBetween(0, 50),
        'country_id' => $faker->numberBetween(3, Country::all()->count()),
    ];
});

$factory->define(App\Challenge::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'description' => $faker->paragraph,
        'to_complete' => $faker->paragraph,
        'approval_status' => 'approved',
        'variable_id' => $faker->numberBetween(1, 196),
    ];
});