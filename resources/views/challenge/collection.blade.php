<ul class="collection">
    @forelse ($challenges as $challenge)
    <li class="collection-item avatar">
        <a href="{{ url('challenge/show', ['id' => $challenge->id]) }}">
            @include('attempt.statusIcon', ['usersAttempt' =>
                $challenge->attempts()->mostRecent()->forCurrentUser()->first()])
            <span class="title">{{ $challenge->title }}</span>
            <p class="truncate">
                {{ $challenge->description }}
            </p>
        </a>
    </li>
    @empty
    <li class="collection-item">
        <span class="title">No challenges to display</span>
    </li>
    @endforelse
</ul>