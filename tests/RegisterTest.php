<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegisterTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testUserRegistration()
    {
        $this->visit('/auth/register')
            ->type('Phil', 'name')
            ->type('new@email.com', 'email')
            ->type('password', 'password')
            ->type('password', 'password_confirmation')
            ->select('3', 'country')
            ->press('Register')
            ->seePageIs('/user/profile');

        $this->seeInDatabase('users', ['email' => 'new@email.com']);
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testUserRegistrationFromEmailLink()
    {
        $this->visit('/auth/register?email=newcomer@email.com')
            ->see("newcomer@email.com");
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testUserInputError()
    {
        $this->visit('/auth/register')
            ->type('Phil', 'name')
            ->type('new@email.com', 'email')
            ->type('password', 'password')
            ->type('password1', 'password_confirmation')
            ->select('3', 'country')
            ->press('Register')
            ->see('The password confirmation does not match.');

        $this->notSeeInDatabase('users', ['email' => 'new@email.com']);
    }
}
