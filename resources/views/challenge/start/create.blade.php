@extends('app')

@section('content')
<div class="container">
    <h1>Create a Challenge</h1>

    <p>It's not just about doing challenges but for every 3 challeges you complete you can create a challenge to inspire others to change their community</p>

    <p>Use the forms below to explore some of the quality of life data for your country and create a challenge relating to it.</p>

    <p>The more people that complete your challenge the greater your impact.</p>

    <div class="row">
        <form class="col s12" method="get" action="{{ url('variable') }}">
            <div class="input-field col s12">
                <select name="topicId">
                    @foreach ($topics as $topic)
                    <option value="{{ $topic->id }}">{{ $topic->name }}</option>
                    @endforeach
                </select>
                <label>Topic</label>
            </div>
            <div class="row">
                <button class="btn waves-effect waves-light" type="submit">Next
                    <i class="mdi mdi-send"></i>
                </button>
            </div>
        </form>
    </div>
</div>
@endsection