<?php
/**
 * Created by IntelliJ IDEA.
 * User: Phil
 * Date: 03/08/2015
 * Time: 08:21
 */

namespace App\Providers;

use App\Country;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
//        view()->composer(
//            'profile', 'App\Http\ViewComposers\ProfileComposer'
//        );

        // Using Closure based composers...
        view()->composer('auth.register', function ($view) {
            $view->with('countries', Country::all()->sort());
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}