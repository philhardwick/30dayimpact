<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ChallengeTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testCreateChallenge()
    {
        $user = factory(App\User::class)->create();
        $user->impact_score = 3;
        $user->save();

        $this->actingAs($user)
            ->visit('/challenge/new-create')
            ->select('4', 'topicId')
            ->press('Next')
            ->press('Create a challenge about this data')
            ->type('title', 'title')
            ->type('My Description', 'description')
            ->type('Complete it this way', 'to_complete')
            ->press('Create')
            ->seePageIs('challenge/created');

        $this->seeInDatabase('challenges', [
            'title' => 'title',
            'description' => 'My Description',
            'to_complete' => 'Complete it this way',
            'owner_id' => $user->id,
        ]);
    }

    public function testCantCreateChallengeBecauseNoImpactPoints() {
        $user = factory(App\User::class)->create();
        $user->impact_score = 0;
        $user->save();

        $this->actingAs($user)
            ->visit('/challenge/new-create')
            ->seePageIs('challenge/no-create')
            ->see('cannot yet create a challenge');
    }

    public function testCantCreateChallengeBecauseNoExtraImpactPointsAfterCreatingChallenge() {
        $user = factory(App\User::class)->create();
        $user->impact_score = 3;
        $user->save();
        $challenge = factory(App\Challenge::class)->create();
        $challenge->owner_id = $user->id;
        $challenge->save();

        $this->actingAs($user)
            ->visit('/challenge/new-create')
            ->seePageIs('challenge/no-create')
            ->see('cannot yet create a challenge')
            ->see('You have created 1 challenge');
    }
}
