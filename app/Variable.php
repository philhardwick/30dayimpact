<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    /**
     * Get the topic for the variable
     */
    public function topic()
    {
        return $this->belongsTo('App\Topic');
    }

    /**
     * Get the variableGroup for the variable
     */
    public function variableGroup()
    {
        return $this->belongsTo('App\VariableGroup');
    }

    /**
     * Get the keywords for the variable
     */
    public function keywords()
    {
        return $this->belongsToMany('App\Keyword');
    }

    /**
     * Get the categories for the variable
     */
    public function categories()
    {
        return $this->hasMany('App\Category');
    }

    /**
     * Get the categories for the variable
     */
    public function challenges()
    {
        return $this->hasMany('App\Challenge');
    }
}
