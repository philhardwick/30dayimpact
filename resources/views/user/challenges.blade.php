@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col s12">
            <h4 class="center-align">Your challenges</h4>
        </div>
        <div class="col s12">
            <ul class="tabs">
                <li class="tab col s6"><a class="active" href="#unfinished_attempts">Unfinished</a></li>
                <li class="tab col s6"><a href="#finished_attempts">Finished</a></li>
                <li class="tab col s6"><a href="#verified_attempts">Verified</a></li>
            </ul>
        </div>
        <div id="unfinished_attempts" class="col s12">
            @include('challenge.collection', ['challenges' => $unfinishedChallenges])
        </div>
        <div id="finished_attempts" class="col s12">
            @include('challenge.collection', ['challenges' => $pendingChallenges])
        </div>
        <div id="verified_attempts" class="col s12">
            @include('challenge.collection', ['challenges' => $verifiedChallenges])
        </div>
    </div>
</div>
@endsection