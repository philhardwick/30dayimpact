<div class="card">
    @if ($attempt->approval_status === 'unfinished')
    <div class="card-content">
        <span class="card-title black-text">Started!</span>
        <p>You started this challenge at {{ $attempt->started }}</p>
    </div>
    <div class="card-action">
        <a href="{{ url('attempt/finish/'.$attempt->id) }}" class="align-center"
           type="submit">Finished?</a>
    </div>
    @else
    <div class="card-image">
        <img src="{{ url($attempt->photo_url) }}">
        <span class="card-title">Completed!</span>
    </div>
    <div class="card-content">
        @yield('cardContent')
        <p>
            The status of this attempt is: {{ $attempt->approval_status }}
        </p>

        <p>{{ $attempt->comment }}</p>
    </div>
    <div class="card-action">
        @yield('cardAction')
    </div>
    @endif
</div>