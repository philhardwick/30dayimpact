@if ($usersAttempt)
    @if ($usersAttempt->approval_status == 'unfinished')
    <i class="mdi mdi-dumbbell circle blue"></i>
    @elseif ($usersAttempt->approval_status == 'pending')
    <i class="mdi mdi-watch circle amber lighten-2"></i>
    @elseif ($usersAttempt->approval_status == 'approved')
    <i class="mdi mdi-check circle green"></i>
    @elseif ($usersAttempt->approval_status == 'rejected')
    <i class="mdi mdi-close-box-outline circle red"></i>
    @endif
@else
<i class="mdi mdi-trophy circle blue"></i>
@endif