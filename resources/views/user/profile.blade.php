@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col s12 m12">
            <div class="row">

                <h5 class="center-align">
                    <i class="mdi mdi-account large"></i><br>{{ $user->name }}
                </h5>
            </div>
            <ul class="collapsible hide-on-med-and-up" data-collapsible="accordion">
                @include('user.collapsible.impact')
                @include('user.collapsible.limit')
            </ul>
            <div class="row hide-on-small-only">
                <div class="col m6">
                    <ul class="collapsible" data-collapsible="accordion">
                        @include('user.collapsible.impact')
                    </ul>
                </div>
                <div class="col m6">
                    <ul class="collapsible" data-collapsible="accordion">
                        @include('user.collapsible.limit')
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row hide-on-med-and-up">
        <div class="col s12">
            <ul class="tabs">
                <li class="tab col s6"><a class="active" href="#unfinished_attempts">Unfinished</a></li>
                <li class="tab col s6"><a href="#attempts_to_verify">To Verify</a></li>
            </ul>
        </div>
        <div id="unfinished_attempts" class="col s12">
            @include('challenge.collection', ['challenges' => $unfinishedChallenges])
        </div>
        <div id="attempts_to_verify" class="col s12">
            @include('challenge.collection', ['challenges' => $challengesToVerify])
        </div>
    </div>

    <div class="row hide-on-small-only">
        <div id="unfinished_attempts" class="col m6">
            <h5>Unfinished</h5>
            @include('challenge.collection', ['challenges' => $unfinishedChallenges])
        </div>
        <div id="attempts_to_verify" class="col m6">
            <h5>To Verify</h5>
            @include('challenge.collection', ['challenges' => $challengesToVerify])
        </div>
    </div>
</div>
@endsection