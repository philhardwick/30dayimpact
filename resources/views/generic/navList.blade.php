@if (Auth::guest())
<li><a href="{{ url('/auth/login') }}">Login</a></li>
<li><a href="{{ url('/auth/register') }}">Register</a></li>
@else
<li>
    <a href="#!" class="dropdown-button" data-activates="challengeDropdown{{$id}}">Challenges<i class="mdi mdi-chevron-down right"></i></a>
    <ul id="challengeDropdown{{$id}}" class="dropdown-content" role="menu">
        <li><a href="{{ url('/challenge/start') }}">Start New</a></li>
        <li><a href="{{ url('/challenge/new-create') }}">Create New</a></li>
        <li><a href="{{ url('/challenge/created') }}">Created</a></li>
    </ul>
</li>
<li>
    <a href="#!" class="dropdown-button" data-activates="userDropdown{{$id}}">{{ Auth::user()->name }}<i class="mdi mdi-chevron-down right"></i></a>
    <ul id="userDropdown{{$id}}" class="dropdown-content">
        <li><a href="{{ url('/user/profile') }}">My Profile</a></li>
        <li><a href="{{ url('/user/challenges') }}">My Challenges</a></li>
        <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
    </ul>
</li>
@endif