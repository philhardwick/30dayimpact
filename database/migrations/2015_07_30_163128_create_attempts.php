<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttempts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attempts', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('started');
            $table->dateTime('finished')->nullable();
            $table->string('photo_url')->nullable();
            $table->string('comment')->nullable();
            $table->enum('approval_status', array('approved', 'pending', 'rejected', 'unfinished'));
            $table->string('approver_email')->nullable();
            $table->integer('challenge_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('challenge_id')->references('id')->on('challenges');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attempts');
    }
}
