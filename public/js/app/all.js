(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $(".dropdown-button").dropdown();
    $('select').material_select();

  }); // end of document ready
})(jQuery); // end of jQuery name space
//# sourceMappingURL=all.js.map