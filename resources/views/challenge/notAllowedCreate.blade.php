@extends('app')

@section('content')
<div class="container">
    <h1>You cannot yet create a challenge</h1>

    <p>You need to have attained 3 impact points for every challenge you want to create</p>

    <p>Currently you have {{ $impactScore }} impact</p>

    <p>You have created {{ $numberOfChallengesCreated }} challenges</p>

</div>
@endsection