<?php

use App\Attempt;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserProfileTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testRedirectToLoginWhenNotAuthenticated()
    {
        $this->visit('/user/profile')
            ->see('Login');
    }

    public function testSeeingProfilePage()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->visit('/user/profile')
            ->see($user->name)
            ->see("Unfinished")
            ->see("To Verify");
    }

    public function testSeeing30DayLimit()
    {
        $user = factory(App\User::class)->create();
        $user2 = factory(App\User::class)->create();

        $challenge = factory(App\Challenge::class)->create();

        $thirtyDaysFromNow = Carbon::now()->addDay(30);

        $this->startANewAttemptForChallenge($user, $challenge);

        $this->visit('/user/profile')
            ->see($thirtyDaysFromNow->toFormattedDateString());

        $this->actingAs($user2)
            ->visit('/user/profile')
            ->dontSee($thirtyDaysFromNow->toFormattedDateString());
    }

    public function testSeeing30DayLimitIsCalculatedCorrectlyFromMultipleAttempts()
    {
        $user = factory(App\User::class)->create();

        $challenge = factory(App\Challenge::class)->create();
        $challenge2 = factory(App\Challenge::class)->create();

        $thirtyDaysFromYesterday = Carbon::now()->addDay(-1)->addDay(30);

        $this->startANewAttemptForChallenge($user, $challenge);

        $this->setUsersAttemptAsStartedYesterday($user);

        $this->startANewAttemptForChallenge($user, $challenge2);

        $this->visit('/user/profile')
            ->see($thirtyDaysFromYesterday->toFormattedDateString());
    }

    private function setUsersAttemptAsStartedYesterday($user)
    {
        $mostRecentAttempt = Attempt::forUser($user->id)->first();
        $mostRecentAttempt->started = Carbon::now()->addDay(-1);
        $mostRecentAttempt->save();
    }

    public function testUserHavingCreatedAChallengeWithNoAttempts() {
        $user = factory(App\User::class)->create();
        $user->impact_score = 3;
        $user->save();

        $this->actingAs($user)
            ->visit('/challenge/new-create')
            ->select('4', 'topicId')
            ->press('Next')
            ->press('Create a challenge about this data')
            ->type('title', 'title')
            ->type('My Description', 'description')
            ->type('Complete it this way', 'to_complete')
            ->press('Create')
            ->visit('/user/profile')
            ->see($user->name);
    }

    /**
     * @param $user
     * @param $challenge
     */
    private function startANewAttemptForChallenge($user, $challenge)
    {
        $this->actingAs($user)
            ->visit('/challenge/show/' . $challenge->id)
            ->press('Challenge Accepted!');
    }
}
