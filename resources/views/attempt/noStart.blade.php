@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col s12">
            <h1>You have already started a challenge today</h1>

            <p>You cannot start more than one challenge on the same day.</p>

            <p>Try starting this challenge tomorrow and keep going with the challenges you already have.</p>
            <a href="{{ url('/user/challenges') }}" class="btn waves-effect waves-light">Go to my Challenges
                <i class="mdi mdi-send"></i>
            </a>
        </div>
    </div>
</div>
@endsection