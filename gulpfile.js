var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');

    mix.scripts([
     'jquery/dist/jquery.min.js',
     'materialize/dist/js/materialize.min.js'
    ], 'public/js', 'resources/assets/bower/');

    //mix.styles([
    // 'mdi/css/materialdesignicons.min.css',
    //], 'public/css', 'resources/assets/bower/');

    mix.scripts([
        'init.js'
    ], 'public/js/app', 'resources/assets/js/');

    mix.copy([
        'resources/assets/bower/materialize/dist/font',
    ], 'public/font');

    mix.copy([
        'resources/assets/bower/mdi/fonts'
    ], 'public/fonts');
});
