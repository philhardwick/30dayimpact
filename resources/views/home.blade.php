@extends('app')

@section('content')
<div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
        <div class="container">
            <br><br>

            <h1 class="header center white-text light">30 Day Impact</h1>

            <div class="row center">
                <h5 class="header col s12 light">Take on the world around you</h5>
            </div>
            <div class="row center">
                <a href="{{ url('challenge/start') }}"
                   class="btn-large waves-effect waves-light primary-color">Start</a>
            </div>
            <br><br>

        </div>
    </div>
    <div class="parallax"><img src="{{ asset('/images/background1.jpg') }}" alt="Unsplashed background img 1"></div>
</div>


<div class="container">
    <div class="section">

        <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center secondary-color-text"><i class="mdi mdi-lightbulb"></i></h2>
                    <h5 class="center">The idea</h5>

                    <p class="light">You will have 30 days from the days you take on your first challenge to have as much impact as possible.
                        You earn an impact point for every challenge you complete that is verified and for everytime someone
                        takes on a challenge you have created.
                    </p>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center secondary-color-text"><i class="mdi mdi-alert"></i></h2>
                    <h5 class="center">Impact</h5>

                    <p class="light">Make a difference in the communities you live in, help people but also help yourself.
                    Challenge others to get involved an change the world around them whilst also going out of your way and doing something
                        you would never usually do!
                    </p>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center secondary-color-text"><i class="mdi mdi-earth"></i></h2>
                    <h5 class="center">Local to you</h5>

                    <p class="light">
                        When you sign up you specify what country you live in. All the data given is from people in your country.
                        You will be acting on answers to questions from the people around you.
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>


<div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center">
                <h5 class="header col s12 light black-text">Go start your first challenge
                    <a href="{{ url('challenge/start') }}"
                       class="btn-large waves-effect waves-light primary-color"><i class="mdi mdi-send"></i></a></h5>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="{{ asset('/images/background2.jpg') }}" alt="Unsplashed background img 2"></div>
</div>

<div class="container">
    <div class="section">

        <div class="row">
            <div class="col s12 center">
                <h3><i class="mdi-trophy secondary-color-text"></i></h3>
                <p class="left-align light">Make sure people know what you're doing and that you're changing the world
                    little by little by sharing on social media. At the end of the 30 days you'll have up to 30 challenges
                    worth of good times to look back on.</p>
            </div>
        </div>

    </div>
</div>


<div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center">
                <h5 class="header col s12 light">Show the effects of open data</h5>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="{{ asset('/images/background3.jpg') }}" alt="Unsplashed background img 3"></div>
</div>
@endsection