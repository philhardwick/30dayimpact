<?php

namespace App\Http\Middleware;

use App\Challenge;
use Closure;
use Illuminate\Support\Facades\Auth;

class AllowCreateChallengeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user) {
            $numberOfChallengesCreated = Challenge::where('owner_id', $user->id)->get()->count();
            $numberOfChallengesAllowedToCreate = floor($user->impact_score/3);
            if ($numberOfChallengesAllowedToCreate <= $numberOfChallengesCreated) {
                return redirect('challenge/no-create');
            }
        }

        return $next($request);
    }
}
