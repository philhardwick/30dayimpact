<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = \App\Variable::find(2)->categories;
        foreach ($countries as $country) {
            \App\Country::create([
                'id' => $country->id,
                'name' => $country->label,
                'country_api_id' => $country->value
            ]);
        }

    }
}
