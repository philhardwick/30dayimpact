<?php

use App\Api\ApiService;
use Illuminate\Database\Seeder;

/**
 * Created by IntelliJ IDEA.
 * User: Phil
 * Date: 06/08/2015
 * Time: 11:52
 */
class ApiTableSeeder extends Seeder
{
    protected $apiService;

    /**
     * TopicTableSeeder constructor.
     */
    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }
}
