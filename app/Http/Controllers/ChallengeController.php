<?php

namespace App\Http\Controllers;

use App\Api\ApiService;
use App\Challenge;
use App\Topic;
use App\Variable;
use Chencha\Share\Share;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class ChallengeController extends Controller
{

    /**
     * Create a new challenge controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('createChallengeAuthorisation', ['only' => ['getNewCreate', 'getCreate', 'postStore']]);
    }

    public function getStart() {
        return view('challenge.start.new', [
            "topics" => Topic::has('variables.challenges')->get(),
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex(Request $request, ApiService $apiService)
    {
        $variables = Variable::where("topic_id", $request->input("topicId"))
            ->has('challenges')->get();
        $data = $apiService->getDataForVariables($variables);
        return view("challenge.chooseNew", [
            "variables" => $variables,
            'data' => $data,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getNewCreate()
    {
        return view("challenge.start.create", [
            "topics" => Topic::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate(Request $request)
    {
        $variable = Variable::find($request->input('variableId'));
        return view("challenge.create", [
            "variable" => $variable
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function postStore(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'to_complete' => 'required',
        ]);

        Variable::find($request->input('variable_id'))->challenges()->create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'to_complete' => $request->input('to_complete'),
            'approval_status' => 'approved',
            'owner_id' => Auth::id(),
        ]);
        return redirect(url('challenge/created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getShow($id, ApiService $apiService)
    {
        $challenge = Challenge::find($id);
        $attemptToVerify = $challenge->attempts()->forUserToVerify(Auth::id())->unverified()->mostRecent()->first();
        $myMostRecentAttempt = $challenge->attempts()->forCurrentuser()->mostRecent()->first();
        $share = new Share;
        $sharingLinks = array();
        if ($myMostRecentAttempt) {
            $hash = bcrypt($myMostRecentAttempt->user->email.$myMostRecentAttempt->comment.$myMostRecentAttempt->challenge->description);
            $sharingLinks = $share->load(url('attempt/show/'.$myMostRecentAttempt->id.'?key='.$hash),
                'I just completed a challenge: '.$challenge->title)
                ->services('facebook', 'gplus', 'twitter');
        }
        return view('challenge.single', [
            'challenge' => $challenge,
            'attemptToVerify' => $attemptToVerify,
            'myMostRecentAttempt' => $myMostRecentAttempt,
            'sharingLinks' => $sharingLinks,
            'data' => $apiService->getDataForVariables($challenge->variable()->get())
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getNoCreate()
    {
        return view('challenge.notAllowedCreate', [
            'impactScore' => Auth::user()->impact_score,
            'numberOfChallengesCreated' => Challenge::where('owner_id', Auth::id())->get()->count(),
        ]);
    }

    /**
     * Show the view of all the challenges the user has created
     * @param ApiService $apiService
     * @return \Illuminate\View\View
     */
    public function getCreated(ApiService $apiService) {
        $variablesTheUserHasCreatedChallengesFor = Challenge::createdByCurrentUser()->get()
            ->map(function($challenge) {
                return $challenge->variable;
            })
            ->unique('id')->values()->all();
        $data = $apiService->getDataForVariables($variablesTheUserHasCreatedChallengesFor);
        return view('challenge.created', [
            'variables' => $variablesTheUserHasCreatedChallengesFor,
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
