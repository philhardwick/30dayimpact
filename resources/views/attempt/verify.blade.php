@extends('attempt.single')

@section('cardContent')
<p>This challenge was started at {{ $attempt->started }} and finished at {{ $attempt->finished }}</p>
<p>
    It's been completed by {{ $attempt->user->name }}
</p>
@endsection

@section('cardAction')
<div class="row">
    <div class="col s6">
        <form action="{{ action('AttemptController@postApproveAttempt', ['id' =>
                            $challenge->attempts()->mostRecent()->forUserToVerify(Auth::user()->id)->first()->id]) }}" method="post">
            {!! csrf_field() !!}
            <button class="btn waves-effect waves-light green" type="submit" name="action">Approve
                <i class="mdi mdi-check"></i>
            </button>
        </form>
    </div>
    <div class="col s6">
        <form action="{{ action('AttemptController@postRejectAttempt', ['id' =>
                        $challenge->attempts()->mostRecent()->forUserToVerify(Auth::user()->id)->first()->id]) }}" method="post">
            {!! csrf_field() !!}
            <button class="btn waves-effect waves-light red" type="submit" name="action">Reject
                <i class="mdi mdi-close"></i>
            </button>
        </form>
    </div>
</div>
@endsection