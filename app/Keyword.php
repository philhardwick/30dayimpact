<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    /**
     * Get the variables for the keyword
     */
    public function variables()
    {
        return $this->belongsToMany('App\Variable');
    }
}
