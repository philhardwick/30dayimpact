@extends('app')

@section('content')
<div class="container">
    <h1>Challenge Start</h1>

    <p>This is where all challenges start. Chose a topic or keyword to get started</p>

    <div class="row">
        <form class="col s12" method="get" action="{{ url('challenge') }}">
            <div class="row">
                <div class="input-field col s12">
                    <select name="topicId">
                        @foreach ($topics as $topic)
                        <option value="{{ $topic->id }}">{{ $topic->name }}</option>
                        @endforeach
                    </select>
                    <label>Topic</label>
                </div>
            </div>
            <div class="col s12">
                <button class="btn waves-effect waves-light" type="submit">Next
                    <i class="mdi mdi-send"></i>
                </button>
            </div>
        </form>
    </div>
</div>
@endsection