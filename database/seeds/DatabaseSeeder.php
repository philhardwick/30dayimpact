<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(TopicTableSeeder::class);
        $this->call(KeywordTableSeeder::class);
        $this->call(VariableGroupTableSeeder::class);
        $this->call(VariableTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(ChallengeTableSeeder::class);

        Model::reguard();
    }
}
