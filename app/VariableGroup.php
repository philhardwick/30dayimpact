<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VariableGroup extends Model
{
    /**
     * Get the variables for the variable group
     */
    public function variables()
    {
        return $this->hasMany('App\Variable');
    }
}
