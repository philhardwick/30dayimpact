@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col s12">
            <h3>Finish Challenge</h3>
        </div>
        @include('generic.errors')
        <form class="col s12" role="form" method="post"
              action="{{ action('AttemptController@postFinishAttempt', ['id' => $attempt->id]) }}"
              enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="file-field input-field">
                <div class="btn">
                    <span><i class="mdi mdi-camera"></i></span>
                    <input type="file" name="photo" value="{{ old('photo') }}"/>
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <textarea id="attempt_comment" class="materialize-textarea" name="comment">{{ old('comment') }}</textarea>
                    <label for="attempt_comment">Comment</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}">
                    <label for="email">Email address of approver</label>

                    <p>(If they aren't a user of 30DayImpact yet, we'll send an email for them to sign up
                        and verify you have completed this challenge)</p>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn waves-effect waves-light" type="submit">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection