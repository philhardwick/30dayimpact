<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    /**
     * Get the variables for the topic
     */
    public function variables()
    {
        return $this->hasMany('App\Variable');
    }

}
