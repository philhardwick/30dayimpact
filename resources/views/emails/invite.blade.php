<h1>You've been invited to join 30DayImpact</h1>

<p>{{ $user->name }} has invited you to join them in their 30DayImpact</p>

<p>They need you to verify that they've completed a challenge, click the link below to sign up and verify their challenge</p>

<a href="{{ url('/auth/register') }}?email={{ $attempt->approver_email }}">Sign up</a>