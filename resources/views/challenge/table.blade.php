<table class="table table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th>Challenge</th>
        <th>Description</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach ($challenges as $challenge)
    <tr>
        <td>{{ $challenge->id }}</td>
        <td>{{ $challenge->title }}</td>
        <td>{{ $challenge->description }}</td>
        <td>
            <a href="{{ url('challenge/show', ['id' => $challenge->id]) }}" class="waves-effect waves-light btn">View</a>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
@if ($challenges->count() == 0)
<p>There are no Challenges to display</p>
@endif