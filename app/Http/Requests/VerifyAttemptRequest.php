<?php

namespace App\Http\Requests;

use App\Attempt;
use Illuminate\Support\Facades\Auth;

class VerifyAttemptRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //Get url param 1
        $attemptId = $this->route('one');

        return Attempt::where('id', $attemptId)
            ->where('approver_email', Auth::user()->email)->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
