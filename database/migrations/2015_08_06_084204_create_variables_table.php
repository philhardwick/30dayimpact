<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variables', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->string('name');
            $table->string('label');
            $table->string('question');
            $table->string('type');
            $table->boolean('is_mean_allowed');
            $table->integer('dataset_id')->unsigned();
            $table->integer('variable_group_id')->unsigned();
            $table->integer('topic_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('variables');
    }
}
