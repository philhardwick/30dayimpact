<?php

namespace App\Http\Requests;

use App\Attempt;
use Illuminate\Support\Facades\Auth;

class FinishAttemptRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //Get url param 1
        $attemptId = $this->route('one');
        return Attempt::where('id', $attemptId)
            ->where('user_id', Auth::id())->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo' => 'required|image|max:1024',
            'email' => 'required|email|not_in:'.Auth::user()->email,
        ];
    }
}
