<?php

use App\Category;
use App\Keyword;
use App\Variable;

class VariableTableSeeder extends ApiTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $variables = $this->apiService->getVariables();
        foreach ($variables as $variable) {
            $insertedVariable = Variable::create([
                "id" => $variable["VariableId"],
                "name" => $variable["VariableName"],
                "label" => $variable["VariableLabel"],
                "question" => $variable["Question"],
                "type" => $variable["VariableType"],
                "is_mean_allowed" => $variable["IsMeanAllowed"],
                "dataset_id" => $variable["DatasetId"],
                "variable_group_id" => $variable["VariableGroup"]["VariableGroupId"],
                "topic_id" => $variable["Topic"]["TopicId"],
            ]);
            foreach ($variable["Categories"] as $category) {
                $category = Category::create([
                    "id" => $category["CategoryId"],
                    "label" => $category["CategoryLabel"],
                    "value" => $category["CategoryValue"],
                    "variable_id" => $category["VariableId"],
                ]);
                $insertedVariable->categories()->save($category);
            }
            foreach ($variable["VariableKeywords"] as $variableKeyword) {
                $insertedVariable->keywords()->attach(Keyword::find($variableKeyword["Keyword"]["KeywordId"]));
            }
        }
    }
}
