@extends('app')

@section('content')
<div class="container">
    <h3>Your created challenges</h3>
    @forelse ($variables as $variable)
    <div class="row">
        <div class="col m12">
            @include('variable.questionAnswer')
            @include('challenge.table', ['challenges' => $variable->challenges()->createdByCurrentUser()->get()])

        </div>
    </div>
    @empty
    <div class="row">
        <div class="col m12">
            <p>You have created no challenges. Every 3 impact points you gain you can create a new challenge. </p>
        </div>
    </div>
    @endforelse
</div>
@endsection