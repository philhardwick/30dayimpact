<?php
/**
 * Created by IntelliJ IDEA.
 * User: Phil
 * Date: 05/08/2015
 * Time: 06:24
 */

namespace App\Api;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Promise;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ResponseInterface;

class ApiService
{
    private $client;


    /**
     * ApiService constructor.
     */
    public function __construct()
    {
        $stack = new HandlerStack();
        $stack->setHandler(new CurlHandler());

        $stack->push(Middleware::mapResponse(function (ResponseInterface $response) {
            if ($response->getStatusCode()) {
                Log::info($response->getReasonPhrase());
            }
            return $response;
        }));

        $this->client = new Client([
            "base_uri" => 'https://api.ukdataservice.ac.uk/V1/datasets/EQLS/',
            'handler' => $stack,
        ]);
    }

    public function getTopics() {
        $response = $this->client->get('topics', ['query' => ['user_key' => config('api.key')]]);
        $json1 = json_decode($response->getBody()->getContents(), true);
        return $json1["Topics"];
    }

    public function getKeywords() {
        $response = $this->client->get('keywords', ['query' => ['user_key' => config('api.key')]]);
        $json1 = json_decode($response->getBody()->getContents(), true);
        return $json1["Keywords"];
    }

    public function getVariableGroups() {
        $response = $this->client->get('variableGroups', ['query' => ['user_key' => config('api.key')]]);
        $json1 = json_decode($response->getBody()->getContents(), true);
        return $json1["VariableGroups"];
    }

    public function getVariablesFromTopic($input)
    {
        $response = $this->client->get('topics/' . $input . '/variables', ['query' => ['user_key' => config('api.key')]]);
        $json1 = json_decode($response->getBody()->getContents(), true);
        return $json1["Variables"];
    }

    public function getVariables() {
        $response = $this->client->get('variables', ['query' => ['user_key' => config('api.key')]]);
        $json1 = json_decode($response->getBody()->getContents(), true);
        return $json1["Variables"];
    }

    public function getDataForVariable($variable) {
        $response = $this->client->get('TimeseriesFrequency', ['query' => [
            'user_key' => config('api.key'),
            'variableId' => $variable->id
        ]]);
        $json1 = json_decode($response->getBody()->getContents(), true);
        return $json1["TimeSeries"];
    }

    public function getDataForVariables($variables)
    {
        //return $this->createFakeResponse($variables);

        $promises = $this->createRequests($variables);
        $results = Promise\unwrap($promises);
        $result = array();
        foreach ($variables as $variable) {
            $jsonResponse = json_decode($results[$variable->id]->getBody()->getContents(), true);
            $totalNumberOfAnswers = 0.0;
            $result[$variable->id] = array();
            foreach ($jsonResponse['TimeSeries'] as $answerForCategory) {
                $totalNumberOfAnswers += $answerForCategory["WeightedFrequency"];
            }
            foreach ($jsonResponse['TimeSeries'] as $answerForCategory) {
                $percentage = $answerForCategory["WeightedFrequency"]/$totalNumberOfAnswers;
                $percentage = number_format($percentage*100, 1);
                $result[$variable->id][$answerForCategory["Value"]] = $percentage;
            }
        }
        return $result;
    }

    /**
     * @param $variables
     * @return array
     */
    private function createFakeResponse($variables)
    {
        $result = array();
        foreach ($variables as $variable) {
            $result[$variable->id] = array();
            for ($i = 0; $i < 12; $i++) {
                $result[$variable->id][$i] = 25;
            }
        }
        return $result;
    }

    /**
     * @param $variables
     * @return array
     */
    private function createRequests($variables)
    {
        $promises = array();
        foreach ($variables as $variable) {
            $promises[$variable->id] = $this->client->getAsync('TimeseriesFrequency', ['query' => [
                'user_key' => config('api.key'),
                'variableId' => $variable->id,
                'startyear' => '2011',
                'filter' => '2:' . Auth::user()->country->country_api_id,
            ]]);
        }
        return $promises;
    }

}