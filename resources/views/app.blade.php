<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>30DayImpact</title>

  <!-- CSS  -->
  <link href="{{ asset('/css/all.css', Request::secure()) }}" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="{{ asset('/css/app.css', Request::secure()) }}" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <nav class="" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="{{ url('/') }}" class="brand-logo white-text light">30DayImpact</a>
      <ul class="right hide-on-med-and-down white-text">
        @include('generic.navList', ['id' => 'desk'])
      </ul>

      <ul id="nav-mobile" class="side-nav grey-text text-darken-2">
        @include('generic.navList', ['id' => 'mob'])
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="mdi mdi-menu"></i></a>
    </div>
  </nav>

  <div>
    @yield('content')
  </div>

  <footer class="page-footer">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text"></h5>

          <p class="grey-text text-lighten-4"></p>


        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        Made by Phil Hardwick
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="{{ asset('/js/all.js', Request::secure()) }}"></script>
  <script src="{{ asset('/js/app/all.js', Request::secure()) }}"></script>

  </body>
</html>
